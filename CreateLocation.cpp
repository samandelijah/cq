#include <cstddef>
#include "CreateLocation.h"
#include "Cafeteria.h"
#include "Gym.h"

Locations* create_location(char loc) {

  switch (loc) {

  case 'C':
    return new Cafeteria();
  case 'G':
    return new Gym();
  default:
    return NULL;
  }
}
