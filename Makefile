# Makefile

CC = g++
CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

cq: main.o Player.h Player.o School.h School.o Locations.h Locations.o CreateLocation.h CreateLocation.o 
	$(CC) main.o Player.o School.o Locations.o CreateLocation.o Cafeteria.o Gym.o -o ./cq

main.o: main.cpp Player.h 
	$(CC) $(CFLAGS) -c main.cpp

Player.o: Player.cpp Player.h School.h Locations.h CreateLocation.h
	$(CC) $(CFLAGS) -c Player.cpp

School.o: School.cpp School.h Locations.h
	$(CC) $(CFLAGS) -c School.cpp

Locations.o: Locations.cpp Locations.h CreateLocation.h Gym.h Cafeteria.h
	$(CC) $(CFLAGS) -c Locations.cpp

CreateLocation.o: CreateLocation.cpp CreateLocation.h Locations.h Cafeteria.h Cafeteria.o Gym.h Gym.o
	$(CC) $(CFLAGS) -c CreateLocation.cpp

Cafeteria.o: Cafeteria.cpp Cafeteria.h Locations.h
	$(CC) $(CFLAGS) -c Cafeteria.cpp

Gym.o: Gym.cpp Gym.h Locations.h
	$(CC) $(CFLAGS) -c Gym.cpp


clean:
	rm -f *.o cq
