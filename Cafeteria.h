// modeled after bishop.h

#ifndef CAFETERIA_H
#define CAFETERIA_H

#include "Locations.h"
#include <string>
#include <vector>

using std::string; using std::vector;

class Cafeteria : public Locations {

 public:

  Cafeteria(); // : Locations(); //{ }
  
  vector<string> get_options();

 private:

  // Cafeteria() : Locations() {}
  vector<string> options;

  friend Locations* create_location(char let);
  
};

#endif
