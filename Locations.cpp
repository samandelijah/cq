#include <vector>
#include <string>
#include "Locations.h"
#include "CreateLocation.h"

bool Locations::add_locations(char let) {

  switch (let) {
    case 'C': case 'G':
      break;
    default:
      return false; // not ready for other letters yet
  }

  locations.push_back(create_location(let));
  return true;
}
