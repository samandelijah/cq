#ifndef GYM_H
#define GYM_H

#include "Locations.h"
#include <string>
#include <vector>

using std::vector; using std::string;

class Gym : public Locations {

public:
  
  Gym();// : Locations();

  vector<string> get_options();
  /*
protected:
  Gym(); // : Locations();
  */
  
private:

  vector<string> options;

  friend Locations* create_location(char let);
  
};

#endif
