#ifndef CREATE_LOCATION_H
#define CREATE_LOCATION_H

#include "Locations.h"

// C for Cafeteria
// G for Gym
// F for Frisbee Club
// A for AEPi
Locations* create_location(char loc);

#endif
