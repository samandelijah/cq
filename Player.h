#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include "School.h"
#include "CreateLocation.h"
#include "Locations.h"

using std::string;

class Player {

 public:

  // Default constructor
  //   Constructs a JHU student named Lance and 10 clout points
  //   for attending a top ten university
  Player();


  /*                         THIS CAN BE FOR THE FUTURE  
  // Input name constructor
  //   Constructs a JHU student with an inputted name and 10 clout points
  //   for attending a top ten university
  Player(string name);
  */

  
  // Get name
  string get_name() { return name; }

  // Get points
  int get_points() { return points; }

  // Get school
  string get_school() { return school; }

  // Add clout points
  void add_points(int num);

  // Subtract clout points
  void sub_points(int num);

 private:

  // Name
  string name;

  // Number of clout points
  int points;

  // Name of school
  string school;

};

#endif
