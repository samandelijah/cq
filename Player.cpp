#include "Player.h"
#include <iostream>
#include <string>

Player::Player() {

  name = "Lance";
  points = 10;
  school = "Johns Hopkins University";

}

/*
Player::Player(string n) {

  name = n;
  points = 10;
  school = "Johns Hopkins University";

}
*/

void Player::add_points(int num) {
  points += num;
}

void Player::sub_points(int num) {
  points -= num;
}
