// designed after board

#ifndef SCHOOL_H
#define SCHOOL_H

#include <string>
#include <iostream>
#include <vector>
#include "Locations.h"
#include "CreateLocation.h"
#include "Cafeteria.h"
#include "Gym.h"


using std::string; using std::vector;

class School {

 public:

  // Constructor with name of school
  School(string school_name);

  // Get name of school
  string get_name() { return name; }

  // Get rank of school
  int get_rank() { return rank; }

  // get locations
  vector<string> get_locations();

 private:

  string name;

  int rank;

  vector<string> locations;
  
};

#endif
  
