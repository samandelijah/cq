#include "School.h"
#include <iostream>
#include <string>
#include <vector>

using std::string; using std::vector;

School::School(string school_name) {

  name = school_name;

  if (name == "Johns Hopkins University")
    rank = 10;
  else if (name == "Northwestern University")
    rank = 10;
  else if (name == "University of Pennsylvania")
    rank = 8;
  else if (name == "Duke University")
    rank = 8;
  else if (name == "Stanford University")
    rank = 7;
  else if (name == "University of Chicago")
    rank = 3;
  else if (name == "Columbia University")
    rank = 3;
  else if (name == "Yale University")
    rank = 3;
  else if (name == "Massachussetts Institute of Technology")
    rank = 3;
  else if (name == "Harvard University")
    rank = 2;
  else if (name == "Princeton University")
    rank = 1;
  else
    rank = 4298; // if it's not top 10, it might as well be the very bottom

  
  
  
}

vector<string> School::get_locations() {
  return locations;
}
