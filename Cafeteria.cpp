// Cafeteria.cpp:

//#include "Locations.h"
#include "Cafeteria.h"
#include <vector>
#include <string>

using std::vector; using std::string;

Cafeteria::Cafeteria() {
  string op1 = "Talk to friends";
  string op2 = "Go to another table";
  string op3 = "Spend another ten minutes at cafeteria";

  options.push_back(op1);
  options.push_back(op2);
  options.push_back(op3);
}

vector<string> Cafeteria::get_options() {

  return options;

}
