//#include "Locations.h"
#include "Gym.h"
#include <vector>
#include <string>

using std::vector; using std::string;

Gym::Gym() {
  string op1 = "Chest day";
  string op2 = "Leg day";
  string op3 = "Back day";
  string op4 = "Core day";
  string op5 = "Arms day";
  string op6 = "Admire self in the mirror";

  options.push_back(op1);
  options.push_back(op2);
  options.push_back(op3);
  options.push_back(op4);
  options.push_back(op5);
  options.push_back(op6);
}

vector<string> Gym::get_options() {

  return options;

}
