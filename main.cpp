#include <iostream>
#include <string>
#include "Player.h"

using std::cout; using std::endl;
using std::string;

int main(void) {

  Player player;

  // bool game_over = false;

  cout << "Hello, " << player.get_name() << ", and welcome to your own quest for clout at " << player.get_school() << "." << endl;

  cout << "Your quest started with " << player.get_points() << ". Good luck, " << player.get_name() << "!" << endl;

  return 0;
}
